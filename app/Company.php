<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    public static $rules = [
        'name' => 'required', 
        'email' => 'required',
        'address' => 'required', 
        'phone' => 'required'            
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'address', 'phone', 'expired_at'];

    public function costs()
    {
        return $this->morphMany('App\Cost', 'costable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roles()
    {
        return $this->hasMany(\App\Role::class, 'company_id');
    }

}
