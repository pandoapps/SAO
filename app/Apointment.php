<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apointment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'apointments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'task_id', 'company_id', 'category_id', 'name', 'start', 'end', 'comment'];

    protected $dates = ['start', 'end'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
