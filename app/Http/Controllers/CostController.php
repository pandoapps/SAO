<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CostRequest;
use App\Http\Controllers\Controller;

use App\Cost;
use App\Category;
use App\Company;
use App\User;
use Auth;

use Carbon\Carbon;
use App\Apointment;

class CostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Recupera os gastos da empresa que o usuário pertence
        // Gastos de empresa
        $costsCompany = Cost::where('costable_type', 'App\Company')
                            ->where('costable_id', Auth::user()->company_id)
                            ->get();
        // Gastos de projeto
        $costsCategories = Cost::join('categories', 'categories.id', '=', 'costs.costable_id')
                    ->where('categories.company_id', Auth::user()->company_id)
                    ->where('costable_type', '=', 'App\Category')
                    ->select('costs.*')
                    ->get();
        // Merge dos gastos
        $costs = $costsCompany->merge($costsCategories);
        return view('costs.index', compact('costs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('company_id', Auth::user()->company_id)->orderBy('name')->lists('name', 'id')->all();
        return view('costs.form',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostRequest $request)
    {
        $input = $request->all();
        if($request->costable_type == "App\Company"){
            $input['costable_id'] = Auth::user()->company_id;
        }
        $input['start'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i',$input['start']);
        if($input['end'] != "")
            $input['end'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i',$input['end']);
        else
            $input['end'] = null;
        Cost::create($input);

        return redirect()->route('cost.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cost = Cost::find($id);
        $category = Category::find($cost->category_id);
        return view('costs.show', compact('cost', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cost = Cost::find($id);
        $categories = Category::where('company_id', Auth::user()->company_id)
                        ->orderBy('name')
                        ->lists('name', 'id')
                        ->all();

        return view('costs.form', compact('cost', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CostRequest $request, $id)
    {
        $cost = Cost::find($id);
        $input = $request->all();
        if($request->costable_type == "App\Company"){
            $input['costable_id'] = Auth::user()->company_id;
        }
        $input['start'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i',$input['start']);
        if($input['end'] != "")
            $input['end'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i',$input['end']);
        else
            $input['end'] = null;

        $cost->fill($input);
        $cost->save();
        return redirect()->route('cost.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cost::where('id', $id)->delete();
        return redirect()->route('cost.index');
    }

    /**
     * Carrega view de relatório
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request)
    {        
        // Se for NULL a conta de usuário é empresa, caso contrário, é um funcionário
        if (Auth::user()->company_id == NULL) {
            // Todos os projetos de uma empresa, com formatação para o select de projetos
            $categories = Category::withTrashed()
                            ->where('company_id', '=', Auth::user()->id)
                            ->orderBy('name')
                            ->lists('name', 'id')
                            ->all();
            
                        // Todos os usuários, com formatação para o select de colaboradores
            $users = User::where('company_id', Auth::user()->id)->orderBy('name')
                        ->get()
                        ->lists('name', 'id')
                        ->all();            
        } else {
            // Todos os projetos de uma empresa, com formatação para o select de projetos
            $categories = Category::withTrashed()->orderBy('name')
                            ->where('company_id', '=', Auth::user()->company_id)
                            ->lists('name', 'id')
                            ->all();
            
            // Todos os usuários, com formatação para o select de colaboradores
            $users = User::where('company_id', Auth::user()->company_id)->orderBy('name')
                        ->get()
                        ->lists('name', 'id')
                        ->all();
        }
        
        // Dados provenientes da view
        $dados = $request->all();

        // Tipos de custo existentes: Variável, Fixo ou Folhe de Pagamento
        $typesCost = array( 'variable' => 'Variável', 
                            'fixed' => 'Fixo',
                            'payroll' => 'Folha de Pagamento');
        
        // Irá conter os custos procurados na tabela costs
        $costs = null;

        // Projeto selecionado
        $category_selected = '';
        // Lembrança para a view de projeto selecionado
        $category_selected_view = '';
        
        // Custo selecionado
        $cost_selected = '';
        // Lembrança para a view de custo selecionado
        $cost_selected_view = '';
        
        // Colaborador selecionado
        $user_selected = null;
        // Lembrança para a view de usuário selecionado
        $user_selected_view = null;

        // Intervalo de tempo selecionado
        $date_range = "";

        // Data inicial do intervalo
        $dateStart = "";

        // Data final do intervalo
        $dateEnd = "";

        // Tabela que irá conter os custos fixo/variável
        $tableDataCost = array();

        // Tabela que irá conter os custos envolvendo usuário
        $tableDataUserCost = array();

        // Tabela que irá conter os custos da empresa do ponto de vista linear
        $tableDataCompanyCostLinear = array();

        // Tabela que irá conter os custos da empresa do ponto de vista Proporcional-Valor
        $tableDataCompanyCostPropValue = array();
        
        // Tabela que irá conter os custos da empresa do ponto de vista Proporcional-Hora
        $tableDataCompanyCostPropHour = array();

        // Tabela que irá conter os custos da empresa do ponto de vista Proporcional-Dia
        $tableDataCompanyCostPropDay = array();

        // Custo dos projetos
        $categoriesCost = array();

        // Custo do projeto selecionado (É utilizado apenas para o
        // plot de gráfico no front-end)
        $categoriesCostSelected =  array();

        // Total de todos os custos de projetos
        $totalCosts = 0;

        // Total de custos na modalidade linear
        $totalCompanyLinear = 0;

        // Total de custos na modalidade proporcional-Valor
        $totalCompanyPropValue = 0;
        
        // Total de custos na modalidade proporcional-Hora
        $totalCompanyPropHour = 0;

        // Total de custos na modalidade proporcional-Dia
        $totalCompanyPropDay = 0;

        // Total dos custos da empresa
        $totalCompanyCosts = 0;

        // Total de todas as horas gastas nos projetos
        $totalH = 0;

        // Total de dias trabalhados
        $totalDays = 0;

        // Caso o método seja GET (primeiro acesso) retorna-se para a view, sem fazer pesquisa alguma
        if($request->isMethod('GET')) {
            return view('costs.reportEmpty', compact(
                'categories', 
                'typesCost', 
                'tableDataCost', 
                'tableDataUserCost', 
                'users', 
                'category_selected', 
                'cost_selected', 
                'user_selected'));
        }

        // Pesquisa para retornar todos os resultados da tabela custo,
        // os quais serão filtrados mais abaixo
        $costs = Cost::orderBy('start', 'ASC');
        
        // Pesquisa para retornar todos os resultados da empresa da tabela apontamentos,
        // os quais serão filtrados mais abaixo
        // Se o usuário for empresa o WHERE se dá pelo ID, caso contrário
        // pelo COMPANY_ID
        if (Auth::user()->company_id == NULL) {
            $apointments = Apointment::orderBy('start', 'ASC')->where('apointments.company_id', Auth::user()->id);
        } else {
            $apointments = Apointment::orderBy('start', 'ASC')->where('apointments.company_id', Auth::user()->company_id);
        }

        // Verifica o intervalo de tempo selecionado
        if(isset($dados['date_range']) && !empty ( $dados['date_range'] )) {
            $date_range = $dados['date_range'];
            $dateRArray = explode(' - ', $dados['date_range']);

            $dateStart = Carbon::createFromFormat('d/m/Y', $dateRArray[0])->startOfDay();

            $dateEnd = Carbon::createFromFormat('d/m/Y', $dateRArray[1])->endOfDay();

            // $totalDays = $dateEnd->diffInDays($dateStart); // Quantidade de dias em int
            
            // Filtro de tempo para custos
            $costs->where('start', '>', $dateStart)          
                ->where(function($query) use ($dateEnd) {
                    $query->where('end', '<=', $dateEnd)
                        ->orWhere('end', '=', NULL);
                })
                ->where('start', '<', $dateEnd);

            // Filtro de tempo para apontamentos
            $apointments->where('start', '>', $dateStart)          
                ->where('end', '<=', $dateEnd);
        }
        
        // É necessário fazer o clone da query para não a sobrepor
        $costsCompany = clone $costs;

        // Custos da empresa do usuário
        // Se o usuário for empresa o WHERE se dá pelo ID, caso contrário
        // pelo COMPANY_ID
        if (Auth::user()->company_id == NULL) {
            $costsCompany->where('costable_id', '=', Auth::user()->id);                                
        } else {
            $costsCompany->where('costable_id', '=', Auth::user()->company_id);
        }

        // Seleciona apenas os custos de empresa
        $costsCompany->where('costable_type', '=', 'App\Company');        

        // Se o tipo do custo for TODOS ou FOLHA DE PAGAMENTO
        if($cost_selected == '' || $cost_selected == 'payroll') {
            // Join de categories, users e $apointments(São selecionados
            // os apontamentos apenas da empresa do usuário)
            $lines = $apointments
                        ->join('categories', 'categories.id', '=', 'apointments.category_id')
                        ->join('users', 'users.id', '=', 'apointments.user_id')
                        ->orderBy('category_id')
                        ->select(   'apointments.*',
                                    'users.name as userName',
                                    'users.HH as userHH',
                                    'users.company_id as usersCompanyId',
                                    'users.fixed_pay as userFixedPay',
                                    'categories.name as categoryName',
                                    'categories.HH as categoryHH',
                                    'categories.created_at as categoryCreatedAt',
                                    'categories.finished_at as categoryFinishedAt')
                        ->get();
            foreach ($lines as $line) {                  
                // Início e fim do apontamento
                $dateStartAp = Carbon::parse($line->start);
                $dateEndAp = Carbon::parse($line->end);

                // Quantidade de minutos trabalhados no projeto
                $min = $dateEndAp->diffInMinutes($dateStartAp);

                // Se há algum valor de hora para o usuário, o salário no
                // projeto é calculado
                if($line->userHH) {
                    // Valor a ser pago
                    $payment = round(($min/60) * ($line->userHH * -1), 2);
                } else {
                    // Caso não haja valor HH do usuário, é posto 0 no valor
                    // pagamento                    
                    $payment = 0;
                }

                // Verifica se já há uma entrada da pessoa na tabela,
                // caso não exista, são criadas as entradas de salário 
                // variável e de total de minutos para o projeto
                if (!isset($tableDataUserCost['users'][$line->userName])) {  
                    $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['totalH'] = 0;
                    $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['var_pay'] = 0;                      
                } 

                // Verifica se a pessoa já possui alguma informação do projeto
                // caso não exista, são criadas as entradas de salário 
                // variável e de total de horas para o projeto
                if (!isset($tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName])) {
                    $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['totalH'] = 0;
                    $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['var_pay'] = 0;                      
                } 
                
                // Pagamento pelo projeto
                $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['var_pay'] += $payment;  

                // Quantidade de horas no projeto
                $tableDataUserCost['users'][$line->userName]['categories'][$line->categoryName]['totalH'] += round(($min/60),2);   

                // Verifica se existe a entrada, para o usuário, 
                // de horas totais e de salário variável total na tabela
                if (!isset($tableDataUserCost['users'][$line->userName]['totalH'])) {
                    $tableDataUserCost['users'][$line->userName]['totalH'] = 0;
                }
                if (!isset($tableDataUserCost['users'][$line->userName]['var_pay_total'])) {
                    $tableDataUserCost['users'][$line->userName]['var_pay_total'] = 0;
                }                                

                // Verifica se existe a entrada de salário fixo na tabela,
                // caso não exista, ela é adicionada
                if (!isset($tableDataUserCost['users'][$line->userName]['fixed_pay'])) {
                    $tableDataUserCost['users'][$line->userName]['fixed_pay'] = ($line->userFixedPay * -1);
                    if($tableDataUserCost['users'][$line->userName]['fixed_pay'] == 0) {
                        $tableDataUserCost['users'][$line->userName]['fixed_pay'] = 0;
                    }
                }

                // Verifica se existe a entrada de salário total na tabela,
                // caso não exista, ela é adicionada (O valor iniciado na
                // coluna é o de salário fixo)
                if (!isset($tableDataUserCost['users'][$line->userName]['pay_total'])) {
                    $tableDataUserCost['users'][$line->userName]['pay_total'] = ($line->userFixedPay * -1);
                }

                // Total de pagamento variável
                $tableDataUserCost['users'][$line->userName]['var_pay_total'] += $payment;

                // Total de horas nos projetos
                $tableDataUserCost['users'][$line->userName]['totalH'] += round(($min/60),2);                
                
                // Total geral da pessoa(Salário variável + fixo)
                $tableDataUserCost['users'][$line->userName]['pay_total'] += $payment;
                
                // Verifica se existe a entrada do projeto na variável
                // que armezena a soma de todos os custos do projeto
                if (!isset($categoriesCost[$line->categoryName])) {
                    $categoriesCost[$line->categoryName]['total_costs'] = 0;
                    $categoriesCost[$line->categoryName]['totalH'] = 0;   
                    $categoriesCost[$line->categoryName]['activeDays'] = $this->calcActiveDays($dateStart, $dateEnd, $line);
                    
                    // Total de dias trabalhado
                    $totalDays += $categoriesCost[$line->categoryName]['activeDays'];

                    // Total da folha de pagamento do projeto
                    $categoriesCost[$line->categoryName]['totalPayroll'] = 0;

                    // Total de custo variável do projeto
                    $categoriesCost[$line->categoryName]['totalVariable'] = 0;

                    // Total de custo fixo do projeto
                    $categoriesCost[$line->categoryName]['totalFixed'] = 0;
                }
                
                // Total de custo e de horas de cada projeto
                $categoriesCost[$line->categoryName]['total_costs'] += $payment;
                $categoriesCost[$line->categoryName]['totalH'] += round(($min/60),2);

                // Total de todos os custos de projetos
                $totalCosts += $payment;

                // Total de todas as horas gastas nos projetos
                $totalH += round(($min/60),2);

                // Total da folha de pagamento do projeto
                $categoriesCost[$line->categoryName]['totalPayroll'] += $payment;
            }
        }

        // Custos de projeto
        // Se o tipo do custo for TODOS, FIXO ou VARIÁVEL
        if($cost_selected != 'payroll') {
            // Apenas os gastos de projetos serão vistos neste relatório
            $costs = $costs
                        ->join('categories', 'categories.id', '=', 'costs.costable_id')
                        ->orderBy('categoryName')
                        ->select(   'costs.*',
                                    'categories.name as categoryName',
                                    'categories.created_at as categoryCreatedAt',
                                    'categories.finished_at as categoryFinishedAt')
                        ->where('costable_type', '=', 'App\Category')
                        ->get();
            foreach ($costs as $cost) {
                array_push($tableDataCost,[
                    'name' => $cost->name,
                    'description' => $cost->description,
                    'origin' => Category::withTrashed()->find($cost->costable_id)->name,
                    'type' => $cost->getTypeNameAttribute(),
                    'value' => $cost->value,
                ]);
                
                // Variável auxiliar na formatação do valor
                // para o cálculo do custo total 
                $value = str_replace('.', '', $cost->value);        
                $value = str_replace(',', '.', $value);

                // Total de todos os custos de projetos
                $totalCosts += floatval($value);

                // Verifica se existe a entrada do projeto na variável
                // que armezena a soma de todos os custos do projeto
                if (!isset($categoriesCost[$cost->categoryName])) {
                    $categoriesCost[$cost->categoryName]['total_costs'] = 0;
                    $categoriesCost[$cost->categoryName]['totalH'] = 0; 
                    $categoriesCost[$cost->categoryName]['activeDays'] = $this->calcActiveDays($dateStart, $dateEnd, $cost); 

                    // Total de dias trabalhado
                    $totalDays += $categoriesCost[$cost->categoryName]['activeDays'];

                    // Total da folha de pagamento do projeto
                    $categoriesCost[$cost->categoryName]['totalPayroll'] = 0;

                    // Total de custo variável do projeto
                    $categoriesCost[$cost->categoryName]['totalVariable'] = 0;

                    // Total de custo fixo do projeto
                    $categoriesCost[$cost->categoryName]['totalFixed'] = 0;
                }
                $categoriesCost[$cost->categoryName]['total_costs'] += floatval($value);  
                
                // Cálculo dos custos fixos e variáveis totais
                if($cost->type == 'variable') {
                    $categoriesCost[$cost->categoryName]['totalVariable'] += floatval($value);
                } else {
                    $categoriesCost[$cost->categoryName]['totalFixed'] += floatval($value);                    
                }
            }
        }

        // Cálculo do impacto (Linear, Proporcional-Valor, Proporcional-HoraTrabalhada, Proporcional-DiaTrabalhado)
        foreach($categoriesCost as $key => $category) {
            // Linear (Calculado a partir da quantidade de dias)
            $categoriesCost[$key]['linear_impact'] = round(1/count($categoriesCost), 4);
            
            // Proporcional-Valor (Calculado a partir do custo de cada projeto)
            if ($totalCosts != 0) {
                $categoriesCost[$key]['propValue_impact'] = round($category['total_costs'] / $totalCosts, 4);            
            } else {
                $categoriesCost[$key]['propValue_impact'] = 0;
            }
            
            // Proporcional-HoraTrabalhada (Calculado a partir da quantidade de horas
            // trabalhadas em cada projeto)
            if ($totalH != 0) {
                $categoriesCost[$key]['propHour_impact'] = round($category['totalH'] / $totalH, 4);                
            } else {
                $categoriesCost[$key]['propHour_impact'] = 0;
            }

            // Proporcional-DiasTrabalhados (Calculado a partir da quantidade de dias
            // trabalhados em cada projeto)
            if ($totalDays != 0) {
                $categoriesCost[$key]['propDays_impact'] = round($category['activeDays'] / $totalDays, 4);                
            } else {
                $categoriesCost[$key]['propDays_impact'] = 0;                                
            }
        }

        // Verifica seleção de usuário (Se for apenas um usuário)
        if(isset($dados['user_id'])  && !empty ( $dados['user_id'] )) {
            $user_selected = User::where('id', '=', $dados['user_id'])->get()[0]->name;

            // Lembrança de estado na view de usuário selecionado
            $user_selected_view = $dados['user_id'];

            // Se existe algum resultado ele é filtrado
            if(count($tableDataUserCost) != 0) {
                $tableDataUserCost['users'] = array_filter($tableDataUserCost['users'], function ($key) use ($user_selected) {
                    return $key == $user_selected;
                }, ARRAY_FILTER_USE_KEY);
            }      
        }

        // Verifica custo selecionado    
        if(isset($dados['cost_type'])  && !empty ( $dados['cost_type'] )) {        
            // Lembrança de estado na view de custo selecionado
            $cost_selected_view = $dados['cost_type'];
            
            // Se for gasto fixo ou variável, faz o filtro dos gastos
            if ($dados['cost_type'] != 'payroll') {
                $cost_selected = Cost::where('type', '=', $dados['cost_type'])->get()->first()->getTypeNameAttribute();
 
                // Se existe algum resultado ele é filtrado
                if(count($tableDataCost) != 0) {
                    $tableDataCost = array_filter($tableDataCost, function ($var) use ($cost_selected) {
                        return $var['type'] == $cost_selected;
                    });
                }     
            }  
            
            // Restringe os custos de empresa para a opção selecionada
            $costsCompany->where('type', '=', $cost_selected_view);
        }

        // Atribuição necessária quando o projeto selecionado é TODOS
        $categoriesCostSelected = $categoriesCost;

        // Verifica o projeto selecionado e calcula os custos da empresa para o projeto selecionado
        if(isset($dados['category_id'])  && !empty ( $dados['category_id'] )) {
            $category_selected = Category::withTrashed()->where('id', '=', $dados['category_id'])->get()->first()->name;
            
            // Lembrança de estado na view de projeto selecionado
            $category_selected_view = $dados['category_id'];

            // Filtra a tabela de custo de projetos de acordo com a opção selecionada em projeto
            if(count($tableDataCost) != 0) {
                $tableDataCost = array_filter($tableDataCost, function ($var) use ($category_selected) {
                    return $var['origin'] == $category_selected;
                });
            }

            // Filtra a tabela de folha de pagamento de acordo com a opção selecionada em projeto
            if(count($tableDataUserCost) != 0) {
                foreach($tableDataUserCost['users'] as $key => $user) {
                    $tableDataUserCost['users'][$key]['categories'] = array_filter($tableDataUserCost['users'][$key]['categories'], function ($var, $key) use ($category_selected) {
                        return $key == $category_selected;
                    }, ARRAY_FILTER_USE_BOTH);
                }
            }

            // Filtra o array de custos de projetos (o qual é utilizado no front-end
            // para o plot de um gráfico)
            if(count($categoriesCost) != 0) {
                $categoriesCostSelected = array_filter($categoriesCost, function ($key) use ($category_selected) {
                    return $key == $category_selected;
                }, ARRAY_FILTER_USE_KEY);
            }

            // Todos os custos da empresa no período
            $costsCompany = $costsCompany->get();

            // Construção da tabela de custos da empresa
            foreach($costsCompany as $cost) {
                // Variável auxiliar na formatação do valor
                // para o cálculo do custo total 
                $value = str_replace('.', '', $cost->value);        
                $value = str_replace(',', '.', $value);

                // Tabela impacto linear
                if (isset($categoriesCost[$category_selected]['linear_impact'])) {
                    array_push($tableDataCompanyCostLinear,[
                        'name' => $cost->name,
                        'type' => $cost->getTypeNameAttribute(),
                        'impact' => $categoriesCost[$category_selected]['linear_impact'],                    
                        'value' => round($value*$categoriesCost[$category_selected]['linear_impact'], 2)
                    ]);

                    // Total de custos na modalidade linear,
                    // para o projeto escolhido
                    $totalCompanyLinear += $value*$categoriesCost[$category_selected]['linear_impact'];
                }
                
                // Tabela impacto Proporcional-Valor
                if (isset($categoriesCost[$category_selected]['propValue_impact'])) {
                    array_push($tableDataCompanyCostPropValue,[
                        'name' => $cost->name,
                        'type' => $cost->getTypeNameAttribute(),
                        'impact' => $categoriesCost[$category_selected]['propValue_impact'],                    
                        'value' => round($value*$categoriesCost[$category_selected]['propValue_impact'], 2)
                    ]);

                    // Total de custos na modalidade proporcional-Valor,
                    // para o projeto escolhido
                    $totalCompanyPropValue += $value*$categoriesCost[$category_selected]['propValue_impact'];
                }

                // Tabela impacto Proporcional-Hora
                if (isset($categoriesCost[$category_selected]['propHour_impact'])) {                
                    array_push($tableDataCompanyCostPropHour,[
                        'name' => $cost->name,
                        'type' => $cost->getTypeNameAttribute(),
                        'impact' => $categoriesCost[$category_selected]['propHour_impact'],                    
                        'value' => round($value*$categoriesCost[$category_selected]['propHour_impact'], 2)
                    ]);

                    // Total de custos na modalidade proporcional-Hora,
                    // para o projeto escolhido
                    $totalCompanyPropHour += $value*$categoriesCost[$category_selected]['propHour_impact'];
                }

                // Tabela impacto Proporcional-Dia
                if (isset($categoriesCost[$category_selected]['propDays_impact'])) {                                
                    array_push($tableDataCompanyCostPropDay,[
                        'name' => $cost->name,
                        'type' => $cost->getTypeNameAttribute(),
                        'impact' => $categoriesCost[$category_selected]['propDays_impact'],                    
                        'value' => round($value*$categoriesCost[$category_selected]['propDays_impact'], 2)
                    ]);

                    // Total de custos na modalidade proporcional-Dia,
                    // para o projeto escolhido
                    $totalCompanyPropDay += $value*$categoriesCost[$category_selected]['propDays_impact'];
                }

                // Total de custos da empresa
                $totalCompanyCosts += $value;
            }
        }
        
        return view('costs.report',
            [
                'categories' => $categories,
                'date_range' => $date_range,
                'typesCost' => $typesCost,
                'users' => $users,
                'category_selected' => $category_selected_view,
                'category_selected_name' => $category_selected,
                'cost_selected' => $cost_selected_view,
                'user_selected' => $user_selected_view,
                'tableDataUserCost' => $tableDataUserCost,
                'tableDataCost' => $tableDataCost,
                'tableDataCompanyCostLinear' => $tableDataCompanyCostLinear,
                'tableDataCompanyCostPropValue' => $tableDataCompanyCostPropValue,
                'tableDataCompanyCostPropHour' => $tableDataCompanyCostPropHour,
                'tableDataCompanyCostPropDay' => $tableDataCompanyCostPropDay,
                'totalCompanyLinear' => $totalCompanyLinear,
                'totalCompanyPropValue' => $totalCompanyPropValue,
                'totalCompanyPropHour' => $totalCompanyPropHour,                
                'totalCompanyPropDay' => $totalCompanyPropDay,                                
                'totalCompanyCosts' => $totalCompanyCosts,
                'categoriesCost' => $categoriesCostSelected,
            ]
        );

    }

    /**
     * Calcula o número de dias ativos de um projeto.
     *
     * @param  \Carbon\Carbon  $dateStart
     * @param  \Carbon\Carbon  $dateEnd
     * @param  Array  $line
     * @return \Illuminate\Http\Response
     */
    public function calcActiveDays ($dateStart, $dateEnd, $line)
    {
        // Cálculo da quantidade de dias ativos no período dado
        $end = $dateEnd;

        // Verifica se a data de criação é 0000-00-00 00:00:00, em caso de
        // sucesso troca a data para 1930-1-1 00:00:00 [Para evitar bug]
        if($line->categoryCreatedAt == '0000-00-00 00:00:00') {
            $start = Carbon::create(1930,1,1,0,0,0);
        } else {
            $start = Carbon::createFromTimestamp(strtotime($line->categoryCreatedAt));
        }

        // Verifica se o início do intervalo escolhido é 
        // maior que o de início do projeto e o substitui 
        // em caso de verdade
        if ($dateStart->greaterThan($start)) {
            $start = $dateStart;
        }

        // Verifica se o projeto finalizou no intervalo
        if ($line->categoryFinishedAt != NULL) {
            $aux = Carbon::createFromTimestamp(strtotime($line->categoryFinishedAt));

            // Troca a data final se o projeto finalizou antes 
            if ($dateEnd->greaterThan($aux)) {
                $end = $aux;
            }
        }

        // Número de dias ativos do projeto
        $activeDays = $start->diffInDays($end);
        return $activeDays;
    }
}
