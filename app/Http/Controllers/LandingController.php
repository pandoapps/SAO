<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Auth;
use App\Company;
use App\User;



class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing.index');                        
    }

    /**
     * Registro de Empresa e administrador
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // Inserção na tabela companies
        $company = new Company;
        $company->name = $request->values['companyName'];
        $company->email = $request->values['companyEmail'];
        $company->address = $request->values['companyAddress'];
        $company->phone = $request->values['companyPhone'];        
        // Gera a data de expiração a partir do dia atual adicionado 1 mês
        $company->expired_at = date(
            'Y-m-d H:i:s', 
            mktime (date("h"), date("i"), date("s"), date("m")+1 , date("d")+1, date("Y"))
        );
        $company->save();

        // Inserção na tabela users
        $user = new User;
        $user->company_id = $company->id;
        $user->name = $request->values['adminName'];
        $user->CH = $request->values['adminCH'];
        $user->email = $request->values['adminEmail'];
        $user->password = bcrypt($request->values['adminPassword']);
        $user->HH = $request->values['adminHH'];                
        $user->fixed_pay = $request->values['adminFixedPay'];
        $user->phone = $request->values['adminPhone'];                                        
        $user->role = $request->values['adminRole'];                                                
        $user->save();

        // Seta o usuário como administrador(Existem 3 perfis gerais)
        // Administrador => 1
        // Gerente => 2
        // Colaborador => 3
        $user->attachRole('2');
                
        return response()->json(array());
    }

}
