<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Permission;
use App\Company;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companyId = \Auth::user()->company_id;
        $roles = Role::where(function($query) use ($companyId) {
                            $query->where('company_id', '=', $companyId)
                                ->orWhere('company_id', '=', NULL);
                        })
                        ->get()
                        ->all();
                
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::orderBy('name','asc')->get()->pluck('name', 'id')->all();
        $permissions = Permission::orderBy('display_name','asc')->get();

        return view('roles.form', compact('companies','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::create($request->all());
        if(isset($request['permissions'])){
            $role->attachPermissions($request['permissions']);
        }

        return redirect(route('role.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $companies = Company::orderBy('name','asc')->get()->pluck('name', 'id')->all();
        $permissions = Permission::orderBy('display_name','asc')->get();

        return view('roles.form', compact('role', 'companies', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $role = Role::find($id);
        $role->update($request->all());
        if(isset($request['permissions'])){
            $role->permissions()->sync($request['permissions']);
        } else {
            $role->permissions()->detach();
        }

        return redirect(route('role.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        return redirect(route('role.index'));
    }
}
