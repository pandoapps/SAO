<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Apointment;

class Task extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'category_id', 'percentage', 'deadline', 'description', 'planned_hours'];

    protected $dates = ['deadline'];

    public function executedHours()
    {
        $apointments = Apointment::where('task_id', $this->id)->get();
        $hours = 0;
        foreach($apointments as $a)
        {
            $hours += (Carbon::parse($a->end)->diffInHours(Carbon::parse($a->start)));
        }
        return $hours;
    }
}
