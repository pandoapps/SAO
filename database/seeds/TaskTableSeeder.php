<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
	private $table = 'tasks';
	
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
    	$dataArray = [
    	['id' => 1, 'name' => 'Projecter', 'category_id' => '1'],
    	['id' => 2, 'name' => 'Projecter P&D', 'category_id' => '1'],
    	['id' => 3, 'name' => 'CREA', 'category_id' => '1'],
        ['id' => 4, 'name' => 'Sistema de ponto', 'category_id' => '1'],
        ['id' => 5, 'name' => 'Rastreamento', 'category_id' => '1'],
        ['id' => 6, 'name' => 'Ingresseiro', 'category_id' => '1'],
        ['id' => 7, 'name' => 'SIGA', 'category_id' => '1'],
        ['id' => 8, 'name' => 'RZX', 'category_id' => '1'],
        ['id' => 9, 'name' => 'Outros', 'category_id' => '1'],

        ['id' => 10, 'name' => 'Prospecção', 'category_id' => '2'],
        ['id' => 11, 'name' => 'Viagens', 'category_id' => '2'],
    	['id' => 12, 'name' => 'Outros', 'category_id' => '2'],

        ['id' => 13, 'name' => 'Contas', 'category_id' => '3'],
        ['id' => 14, 'name' => 'Banco', 'category_id' => '3'],
        ['id' => 15, 'name' => 'Suporte', 'category_id' => '3'],

        ['id' => 16, 'name' => 'Café', 'category_id' => '4'],
        ['id' => 17, 'name' => 'Almoço', 'category_id' => '4'],
        ['id' => 18, 'name' => 'Compromissos Pessoais', 'category_id' => '4'],
    	];

    	DB::table($this->table)->insert($dataArray);
    }
}
