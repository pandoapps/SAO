<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
	private $table = 'companies';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            ['id' => 1, 'name' => 'Pandô Apps']
        ];

        DB::table($this->table)->insert($dataArray);
    }
}
