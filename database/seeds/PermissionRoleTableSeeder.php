<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
	private $table = 'permission_role';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            # admin
            ['permission_id' => 1,  'role_id' => 1],
            ['permission_id' => 2,  'role_id' => 1],
            ['permission_id' => 3,  'role_id' => 1],
            ['permission_id' => 4,  'role_id' => 1],
            ['permission_id' => 5,  'role_id' => 1],

            # manager                        
            ['permission_id' => 1,  'role_id' => 2],
            ['permission_id' => 3,  'role_id' => 2],
            ['permission_id' => 4,  'role_id' => 2],
        ];

        DB::table($this->table)->insert($dataArray);
    }
}