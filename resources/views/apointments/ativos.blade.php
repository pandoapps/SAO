@extends('layouts.master') @section('content')

<div class="container-fluid" style="padding-right: 70px; padding-left: 40px">

	<div style="display: block; height: 90px">
		<div style="float: left;">
			<h2>DASHBOARD</h2>
		</div>
	</div>
	<p>
		<table id="apointments-table" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Usuário</th>
					<th>Início</th>
					<th>Fim</th>
					<th>Projeto</th>
					<th>Tarefa</th>
					<th>Última atualização</th>
					<th>Ações</th>
				</tr>
			</thead>
		</table>
	</p>
</div>

@endsection @section('inline_scripts')
<script>
	$(function () {
		var apointments_table = $('#apointments-table').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
			},
			processing: true,
			serverSide: false,
			ajax: '{!! route('apointment.ajax_ativos') !!}',
			columns: [{
					data: 'user_name',
					name: 'users.name'
				},
				{
					data: 'start_f',
					name: 'apointments.start'
				},
				{
					data: 'end_f',
					name: 'apointments.end'
				},
				{
					data: 'category_name',
					name: 'categories.name'
				},
				{
					data: 'task_name',
					name: 'tasks.name'
				},
				{
					data: 'updated_f',
					name: 'apointments.updated_at'
				},
				{
					sortable: false,
					"render": function (data, type, full, meta) {
						var buttonID = full.id;
						var buttonIdUser = full.user_id; 
						var btnEdit = '';
						var btnCheck = '';
						var btnDelete = '';
						
						if(full.user_id=={{\Auth::user()->id}}){
							btnEdit = '<a id=edt_' + buttonID + ' href="/apontamentos/editar/' + buttonID + '" data-id = ' + buttonID +
								' class="edtBtn fa fa-pencil fa-2x" title="editar" role="button"></a>&nbsp;&nbsp;';
							btnCheck = '<a id=check_' + buttonID + ' data-id = ' + buttonID +
								' class="checkBtn fa fa-check-square-o  fa-2x" role="button"></a>&nbsp;&nbsp;';
							btnDelete = '<a id=del_' + buttonID + ' data-id = ' + buttonID + ' user-id = ' + buttonIdUser  + 
								' class="delBtn fa fa-trash-o fa-2x" role="button"></a>';
						}

						return btnEdit + btnCheck + btnDelete;
					}
				}
			]
		});

		$(document).on('click', '.checkBtn', function () {
			var id = $(this).attr("data-id");
			swal({
				title: 'Deseja encerrar o apontamento?',
				text: "Deixe um comentário abaixo se desejar",
				input: 'textarea',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Encerrar!',
				cancelButtonText: 'Cancelar',
				allowOutsideClick: false
			}).then(function (textarea) {
				$.ajax({
					headers: {
						'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: 'POST',
					url: '{!! route('apointment.check') !!}',
					data: {id:id, comment:textarea,},
					enctype: 'multipart/form-data',
					success: function(data){
						if(!data.success) {
							if(data.gt_eight_hours){
								swal({
									title: 'Deseja realmente registrar o apontamento com mais de 8h?',
									text: "O apontamento que você está registrando é de "+data.status,
									type: 'question',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: 'Sim!',
									cancelButtonText: 'Cancelar',
									allowOutsideClick: false
								}).then(function(){
									$.ajax({
										headers: {
											'X-CSRF-Token': '{!! csrf_token() !!}'
										},
										type: 'POST',
										url: '{!! route('apointment.check') !!}',
										data: {
											id: id,
											comment: textarea,
											gt_eight_hours: true
										},
										enctype: 'multipart/form-data',
										success: function (data) {
											swal(
												'Apontamento encerrado!',
												'Horas trabalhadas: ' + data.status,
												'success'
											);
											apointments_table.ajax.reload();
										},
										error: function () {
											swal(
												'Cancelado',
												'O apontamento não foi encerrado',
												'error'
											)
										}
									});
								});
							}
							else{
								swal(
									'Apontamento não encerrado',
									data.message,
									'error'
								);
							}
						} else {
								swal(
									'Apontamento encerrado!',
									'Horas trabalhadas: ' + data.status,
									'success'
								);
						}
						apointments_table.ajax.reload();
					},
					error: function () {
						swal(
							'Cancelado',
							'O apontamento não foi encerrado',
							'error'
						)
					}
				});
			});

		});

		$(document).on('click', '.delBtn', function () {
			var id = $(this).attr("data-id");
			var user_id = $(this).attr("user-id");

			swal({
				title: 'Tem certeza?',
				text: "Você não poderá desfazer essa ação!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Excluir!',
				cancelButtonText: 'Cancelar'
			}).then(function () {

				$.ajax({
					headers: {
						'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: 'POST',
					url: '{!! route('apointment.destroy') !!}',
					data: {
						id: id,
						user_id: user_id
					},
					enctype: 'multipart/form-data',
					success: function (data) {
						if (data.status == true) {
							swal(
								'Deletado!',
								'O apontamento foi excluido.',
								'success'
							);
							apointments_table.ajax.reload();
						} else {
							swal(
								'Oops...',
								'Não realizar essa ação, tente novamente.',
								'error'
							)
						}
					},
					error: function () {
						swal(
							'Cancelado',
							'O apontamento não foi excluido',
							'error'
						)
					}
				});
			}, function (dismiss) {
				// dismiss can be 'cancel', 'overlay',
				// 'close', and 'timer'
				if (dismiss === 'cancel') {
					swal(
						'Cancelado',
						'O apontamento não foi excluido',
						'error'
					)
				}
			});

		});

	});
</script>

@endsection