@extends('layouts.master')

@section('content')
<style>
	.box_create {
		width: 90%;
		margin: auto;
	}

	.box_create input {
		margin: 15px 0px;
	}

	html, body {
		height: 100%;
	}

	.wrapper {
		background-color: transparent !important;
	}
}
</style>

<div class="container-fluid">
	<div class="wrapper">
		<div class="box box-primary box-create">
			<div class="box-header with-border">
				<h3 class="box-title">Tarefas: <b>{{$task->name}}</b></h3>
				<div class="box-tools">
					<a href="{{route('task.edit', ['id' => $task->id])}}" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
				</div>
			</div>
			<div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td width="10%"><b>Deadline:</b></td>
                            <td>{{$category->name}}</td>
                        </tr>
                        <tr>
                        	<td><b>Horas (P/E)</b></td>
                        	<td>{{(isset($task->plannedHours)?$task->plannedHours : 0)}}/{{$task->executedHours()}}</td>
                        </tr>
                        <tr>
                            <td><b>Porcentagem:</b></td>
                            <td>{{$task->percentage}}%</td>
                        </tr>
                        <tr>
                            <td><b>Descrição:</b></td>
                            <td>{!!$task->description!!}</td>
                        </tr>

                    </tbody>
                </table>
                
            </div>
		</div>
	</div>
</div>
@endsection

@section('inline_scripts')

@endsection