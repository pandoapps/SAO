@extends('layouts.master')

@section('content')

<div class="container-fluid" style="padding-right: 70px; padding-left: 40px">	
	
	<div style="display: block; height: 90px">
		<div style="float: left;"><h2>Minhas Tarefas</h2></div>
		<div style="float: right;">
			<h2>
				<a class="fa fa-plus-square" role="button" href="{{ route('task.create') }}"></a>
			</h2>
		</div>
	</div>
	<table id="tasks-table" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th width="20%">Projeto</th>
				<th>Nome</th>
				<th width="5%">Horas(P/E)</th>
				<th width="5%">%</th>
				<th width="15%">Deadline</th>
				<th width="11%">Ações</th>
			</tr>
		</thead>
	</table>
</div>

@endsection

@section('inline_scripts')
<script>
	$(function() {
		var apointments_table = $('#tasks-table').DataTable({
			"language": {
				"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
			},
			processing: true,
			serverSide: false,
			ajax: '{!! route('task.ajax_tasks') !!}',
			searching: true,
			columns: [
			{ data: 'category', name: 'category'},
			{ data: 'task', name: 'taskname'},
			{ data: 'hours', name: 'hours'},
			{ data: 'percentage', name: 'percentage'},
			{ data: 'deadline', name: 'deadline'},
			{
				sortable: false,
				"render": function ( data, type, full, meta ) {
					var buttonID = full.idTask;
					btnShow = '<a id=show_'+buttonID+' href="/tasks/'+buttonID+'" class="showBtn fa fa-info-circle fa-2x" title="Detalhes" role="button"></a>&nbsp;&nbsp;';
					btnEdit = '<a id=edt_'+buttonID+' href="/tasks/editar/'+buttonID+'" data-id = '+buttonID+' class="edtBtn fa fa-pencil fa-2x" title="editar" role="button"></a>&nbsp;&nbsp;';
					btnDelete = '<a id=del_'+buttonID+' data-id = '+buttonID+' class="delBtn fa fa-trash-o fa-2x" role="button"></a>';

					return btnShow + btnEdit + btnDelete;
				}
			}
			]
		});

		$(document).on('click', '.delBtn', function() {
			var id = $(this).attr("data-id");
			swal({
				title: 'Tem certeza?',
				text: "Você não poderá desfazer essa ação e todos os apontamentos ficarão sem tarefas!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Excluir!',
				cancelButtonText: 'Cancelar'
			}).then(function () {

				$.ajax({
					headers: {
						'X-CSRF-Token': '{!! csrf_token() !!}'
					},
					type: 'POST',
					url: '{!! route('task.destroy') !!}',
					data: {id:id},
					enctype: 'multipart/form-data',
					success: function(data){
						if(data.status == true) {
							swal(
								'Deletado!',
								'A tarefa foi excluida com sucesso.',
								'success'
								);
							apointments_table.ajax.reload();
						} else {
							swal(
								'Oops...',
								'Não realizar essa ação, tente novamente.',
								'error'
								)
						}
					},
					error: function(){
						swal(
							'Cancelado',
							'A tarefa não foi excluida',
							'error'
							)
					}
				});	
			}, function (dismiss) {
					  // dismiss can be 'cancel', 'overlay',
					  // 'close', and 'timer'
					  if (dismiss === 'cancel') {
					  	swal(
					  		'Cancelado',
					  		'A tarefa não foi excluida',
					  		'error'
					  		)
					  }
					});

		});

	});


</script>

@endsection